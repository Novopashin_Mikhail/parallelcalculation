using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParallelCalculation
{
    public class MyParallelSum
    {
        public int ParallelSum(int[] arr)
        {
            var parts = Program.ParallelParts;
            var count = arr.Length;
            var partSize = count / parts;

            var lastPart = 0;
            var rest = count % parts;
            if (rest != 0)
            {
                lastPart += rest;
            }
            
            var taskList = new List<Task<int>>();

            for (int i = 0; i < parts; i++)
            {
                var j = i;
                var task = Task.Run(() => arr.Skip(j * partSize).Take(partSize + lastPart).Sum());
                taskList.Add(task);
            }

            var taskArray = taskList.ToArray();
            var taskWhenAll = Task.WhenAll(taskArray);
            taskWhenAll.GetAwaiter().GetResult();

            return taskList.Sum(taskItem => taskItem.Result);
        }
        
        public int ParallelLinqSum(int[] arr)
        {
            return arr.AsParallel().Sum(x => x);
        }

        public int SimpleSum(int[] arr)
        {
            return arr.Sum();
        }
    }
}