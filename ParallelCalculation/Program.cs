﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace ParallelCalculation
{
    class Program
    {
        static readonly Stopwatch StopWatch = new Stopwatch();
        public static int ParallelParts = 0;
        
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("settings.json");
            
            var config = builder.Build();
            ParallelParts = int.Parse(config["parallelParts"]);
            
            var sum = new MyParallelSum();

            var arr100_000 = CreateArray(100_000);
            var arr1_000_000 = CreateArray(1_000_000);
            var arr10_000_000 = CreateArray(10_000_000);

            Console.WriteLine("SimpleSum:");
            ShowingTime(sum.SimpleSum, arr100_000);
            ShowingTime(sum.SimpleSum, arr1_000_000);
            ShowingTime(sum.SimpleSum, arr10_000_000);
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine("ParallelSum:");
            ShowingTime(sum.ParallelSum, arr100_000);
            ShowingTime(sum.ParallelSum, arr1_000_000);
            ShowingTime(sum.ParallelSum, arr10_000_000);
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine("ParallelLinqSum:");
            ShowingTime(sum.ParallelLinqSum, arr100_000);
            ShowingTime(sum.ParallelLinqSum, arr1_000_000);
            ShowingTime(sum.ParallelLinqSum, arr10_000_000);
        }

        static void ShowingTime(Func<int[], int> func, int[] arr)
        {
            StopWatch.Start();
            var sum = func(arr);
            StopWatch.Stop();
            Console.WriteLine($"SizeArray:{arr.Length}; Sum:{sum}; EllapseTime:{StopWatch.Elapsed.Milliseconds}");
            StopWatch.Reset();
        }

        static int[] CreateArray(int count)
        {
            var random = new Random();
            var list = new List<int>();
            for (int i = 0; i < count; i++)
            {
                list.Add(random.Next(1, 100));
            }

            return list.ToArray();
        }
    }
}